#include <algorithm>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <cfenv>

static const double q = -100;

static const double K = 1.e-12;
static const double phi = 0.4;
static const double lambda_dry = 0.5;
static const double lambda_wet = 0.5;
static const double lambda_GR = 0.2;
static const double lambda_LR = 0.5;
static const double lambda_SR = 1.0;
static const double DpmG = 2.6e-6;

static const double rhoLR = 1000.;

static const double muLR = 2.938e-4;
static const double muC_GR = 1.8e-5;
static const double muW_GR = 1.8e-5;

// Saturation
static const double sL_res = 0.0;
static const double sG_res = 0.0;
static const double sL_max = 1. - sG_res;

#if 1
static const double k_r_G_min = 1.e-5;
static const double k_r_L_min = 1.e-5;
#else
static const double k_r_G_min = 0;
static const double k_r_L_min = 0;
#endif

static const double MW = 0.018016;
static const double MC = 0.028949;
static const double R = 8.3144621;
static const double dh_evap = 2258000;

static const double p_ref = 101325;
static const double T_ref = 373.15;

// Brooks-Corey parameters
static const double p_thr_BC = 5.e3;
static const double exp_BC = 3.0;
// Exponential parameters
static const double pCap_max = 5.0e3;
static const double exp_E = 3.5;

enum RetentionCurve
{
    Brooks_Corey,
    Leverett,
    Exponential,
    Fatt
};

enum ThermalConductivityModel
{
    VolumeFractionDependent,
    SaturationDependent
};

RetentionCurve characteristic_curve = Brooks_Corey;
RetentionCurve relative_permeability_model = Brooks_Corey;

ThermalConductivityModel lambda_model = VolumeFractionDependent;

double capillary_pressure(const double sL_eff)
{
    switch (characteristic_curve)
    {
    case Brooks_Corey:
    {
        return p_thr_BC * std::pow(sL_eff, -1. / exp_BC);
    }
    case Leverett:
    {
        auto const p0 = std::sqrt(phi / K);
        const double a = 1.419;
        const double b = -2.12;
        const double c = 1.263;

        const double gamma = .05878;
        auto const sG_eff = 1. - sL_eff;
        return std::max(0., p0 * gamma * (a * sG_eff + b * std::pow(sG_eff, 2.) + c * std::pow(sG_eff, 3.)));
    }
    case Exponential:
    {
        return pCap_max * std::pow(1. - sL_eff, 1. / exp_E);
    }
    default:
        exit(0);
    }
}

double capillary_pressure_derivative(const double sL_eff)
{
    switch (characteristic_curve)
    {
    case Brooks_Corey:
    {
        return -p_thr_BC / exp_BC * std::pow(sL_eff, -(exp_BC + 1) / exp_BC);
    }
    case Leverett:
    {
        auto const p0 = std::sqrt(phi / K);
        const double a = 1.419;
        const double b = -2.12;
        const double c = 1.263;

        const double gamma = .05878;
        auto const sG_eff = 1. - sL_eff;
        return -p0 * gamma * (a - sG_eff * (-3 * c * sG_eff - 2 * b));
    }
    case Exponential:
    {
        return -pCap_max / exp_E * std::pow(1. - sL_eff, (1. / exp_E) - 1.);
    }
    default:
        exit(0);
    }

    return 0.;
}

double saturation_effective(const double pCap)
{
    switch (characteristic_curve)
    {
    case Brooks_Corey:
    {
        return std::pow(pCap / p_thr_BC, -exp_BC);
    }
    case Leverett:
    {
        std::cout << "Saturation Leverett is not implemented.\n";
        exit(0);
    }
    case Exponential:
    {
        return 1. - std::pow(pCap / pCap_max, exp_E);
    }
    default:
        exit(0);
    }
}

double relative_permeability_gas(const double sL_eff)
{
    switch (relative_permeability_model)
    {
    case Fatt:
        return std::max(k_r_G_min, std::pow(1. - sL_eff, 3.));
    case Brooks_Corey:
        return std::max(k_r_G_min, (1. - sL_eff) * (1. - sL_eff) *
                                       (1. - std::pow(sL_eff, (2. + exp_BC) / exp_BC)));
    default:
        exit(0);
    }
}

double relative_permeability_liquid(const double sL_eff)
{
    switch (relative_permeability_model)
    {
    case Fatt:
        return std::max(k_r_L_min, std::pow(sL_eff, 3.));
    case Brooks_Corey:
        return std::max(k_r_L_min, std::pow(sL_eff, (2. + 3. * exp_BC) / exp_BC));
    default:
        exit(0);
    }
}

double vapour_pressure(const double p_sat, const double pGR, const double pCap, const double xnCG, const double T)
{
    //    return p_sat * std::exp(-(pCap - xnCG * pGR) * MW / rhoLR / R / T);
    return p_sat * std::exp(-(pCap - 0. * pGR) * MW / rhoLR / R / T);
}

double saturation_vapour_pressure(const double T)
{
    return p_ref * std::exp((1. / T_ref - 1. / T) * dh_evap * MW / R);
}

double partial_pressure_vapour(const double pGR, const double pCap, const double xnCG, const double T)
{
    const double p_sat = saturation_vapour_pressure(T);
    return vapour_pressure(p_sat, pGR, pCap, xnCG, T);
}

double molar_mass_gas_phase(const double xnCG)
{
    return xnCG * MC + (1. - xnCG) * MW;
}

double density_gas_phase(const double pGR, const double xnCG, const double T)
{
    const double M = molar_mass_gas_phase(xnCG);
    return pGR * M / R / T;
}

double viscosity_gas_phase(const double xnCG)
{
    return xnCG * muC_GR + (1. - xnCG) * muW_GR;
}

double dynamic_viscosity_gas_phase(const double pGR, const double xnCG, const double T)
{
    const double muGR = viscosity_gas_phase(xnCG);
    const double rhoGR = density_gas_phase(pGR, xnCG, T);
    return muGR / rhoGR;
}

double thermal_conductivity(const double sL_eff)
{
    switch (lambda_model)
    {
    case SaturationDependent:
    {
        return lambda_dry + std::sqrt(sL_eff) * (lambda_wet - lambda_dry);
    }
    case VolumeFractionDependent:
    {
        auto const sL = sL_eff * (sL_max - sL_res) + sL_res;
        auto const phiG = (1. - sL) * phi;
        auto const phiL = sL * phi;
        auto const phiS = 1. - phi;
        return lambda_GR * phiG + lambda_LR * phiL + lambda_SR * phiS;
    }
    default:
        std::cout << "No valid Thermal conductivity model.\n";
        exit(0);
    }
}

double beta_(const double pGR, const double xnCG, const double T)
{
    const double nuGR = dynamic_viscosity_gas_phase(pGR, xnCG, T);
    const double nuLR = muLR / rhoLR;
    return nuLR / nuGR;
}

double xi_(const double sL_eff, const double pGR, const double xnCG, const double T)
{
    const double krG = relative_permeability_gas(sL_eff);
    const double krL = relative_permeability_liquid(sL_eff);
    const double beta = beta_(pGR, xnCG, T);
    const double xnWG = 1. - xnCG;

    const double a = 1. + ((rhoLR * R * T) / (pGR * MW * xnWG));

    return a / krG + beta / krL;
}
double diffusivity(const double sL_eff)
{
    return phi * (1. - sL_eff) * DpmG;
}

double zeta_(const double sL_eff, const double pGR, const double xnCG, const double T)
{
    const double muGR = viscosity_gas_phase(xnCG);

    const double a = K * rhoLR * R * T * xnCG;
    const double xnWG = 1. - xnCG;
    const double D = diffusivity(sL_eff);
    const double b = muGR * D * MW * xnWG;
    const double c = (pGR * MW) / (rhoLR * R * T) + (1. / xnWG);

    return (a / b) * c;
}

double alpha_(const double sL_eff, const double pGR, const double xnCG)
{
    const double pCap = capillary_pressure(sL_eff);
    return 1. + ((pCap - 0. * pGR) / (dh_evap * rhoLR));
}

double delta_(const double sL_eff, const double pGR, const double xnCG, const double T)
{
    const double nuGR = dynamic_viscosity_gas_phase(pGR, xnCG, T);
    const double lambda = thermal_conductivity(sL_eff);
    const double alpha = alpha_(sL_eff, pGR, xnCG);

    return rhoLR * dh_evap * dh_evap * K * alpha / (lambda * nuGR * T);
}

double eta_(const double sL_eff, const double pGR, const double xnCG, const double T)
{
    const double delta = delta_(sL_eff, pGR, xnCG, T);
    const double xi = xi_(sL_eff, pGR, xnCG, T);
    const double zeta = zeta_(sL_eff, pGR, xnCG, T);

    return delta / (delta + xi + zeta);
}

double gamma_(const double sL_eff, const double pGR, const double xnCG, const double T)
{
    const double krG = relative_permeability_gas(sL_eff);
    const double krL = relative_permeability_liquid(sL_eff);

    const double beta = beta_(pGR, xnCG, T);
    const double xnWG = 1. - xnCG;

    return 1. / krG * (1. / xnWG + beta * krG / krL);
}

// 1d-domain coordinate over effective saturation
double dz_dsL_eff(const double sL_eff, const double pGR, const double xnCG, const double T)
{
    const double dpCap_dsL = capillary_pressure_derivative(sL_eff);
    const double eta = eta_(sL_eff, pGR, xnCG, T);

    const double nuGR = dynamic_viscosity_gas_phase(pGR, xnCG, T);
    const double omega = q * nuGR / (dh_evap * K);

    const double gamma = gamma_(sL_eff, pGR, xnCG, T);

    return -dpCap_dsL / (eta * omega * gamma);
}

// gas pressure over effective saturation
double dpGR_dsL_eff(const double sL_eff, const double pGR, const double xnCG, const double T)
{
    const double dpCap_dsL = capillary_pressure_derivative(sL_eff);
    const double krG = relative_permeability_gas(sL_eff);
    const double xnWG = 1. - xnCG;
    const double gamma = gamma_(sL_eff, pGR, xnCG, T);

    return dpCap_dsL / (krG * xnWG * gamma);
}

// air molar fraction over effective saturation
double dxnCG_dsL_eff(const double sL_eff, const double pGR, const double xnCG, const double T)
{
    const double dpCap_dsL = capillary_pressure_derivative(sL_eff);
    const double muGR = viscosity_gas_phase(xnCG);
    const double xnWG = 1. - xnCG;
    const double gamma = gamma_(sL_eff, pGR, xnCG, T);
    const double D = diffusivity(sL_eff);

    return -dpCap_dsL * K / (muGR * D) * xnCG / xnWG / gamma;
}

// temperature over effective saturation
double dT_dsL_eff(const double sL_eff, const double pGR, const double xnCG, const double T)
{
    const double dpCap_dsL = capillary_pressure_derivative(sL_eff);
    const double eta = eta_(sL_eff, pGR, xnCG, T);
    const double nuGR = dynamic_viscosity_gas_phase(pGR, xnCG, T);
    const double lambda = thermal_conductivity(sL_eff);
    const double gamma = gamma_(sL_eff, pGR, xnCG, T);

    return dpCap_dsL * (1 - eta) / eta * dh_evap / (nuGR * lambda) * K / gamma;
}

// temperature in equilibrium with gas pressure, saturation and air molar fraction
double equilibrium_temperature(const double sL_eff, const double pGR, const double xnCG)
{
    auto const pCap = capillary_pressure(sL_eff);
    auto const a = T_ref * (1 + (pCap - 0. * pGR) / (dh_evap * rhoLR));
    auto const b = 1 - T_ref * R / dh_evap / MW * std::log(pGR * (1 - xnCG) / p_ref);
    return a / b;
}

struct Y
{
    double z;
    double pGR;
    double xnCG;
    double T;

    Y operator+(const Y y) const
    {
        return {z + y.z, pGR + y.pGR, xnCG + y.xnCG, T + y.T};
    }

    Y &operator+=(const Y y)
    {
        z += y.z;
        pGR += y.pGR;
        xnCG += y.xnCG;
        T += y.T;

        return *this;
    }

    static void print_head()
    {
        std::cout << "z,sL_eff,sL,pGR,xnCG,T,pCap,eta,kRelG,kRelL,pVap,rhoGR\n";
    }

    void print(const double sL_eff)
    {
        double const pCap = capillary_pressure(sL_eff);

        std::cout << z
                  << ',' << sL_eff
                  << ',' << sL_eff * (1. - sG_res - sL_res) + sL_res
                  << ',' << pGR
                  << ',' << xnCG
                  << ',' << T
                  << ',' << pCap
                  << ',' << eta_(sL_eff, pGR, xnCG, T)
                  << ',' << relative_permeability_gas(sL_eff)
                  << ',' << relative_permeability_liquid(sL_eff)
                  << ',' << vapour_pressure(saturation_vapour_pressure(T), pGR, pCap, xnCG, T)
                  << ',' << density_gas_phase(pGR, xnCG, T)
                  << '\n';
    }
};

Y operator*(const Y y, const double a)
{
    return {a * y.z, a * y.pGR, a * y.xnCG, a * y.T};
}

Y operator*(const double a, const Y y)
{
    return y * a;
}

Y eval_dydt(const double sL_eff /* ^= time */, const Y y)
{
    // dsL_eff = step size = omitted
    return {
        dz_dsL_eff(sL_eff, y.pGR, y.xnCG, y.T),
        dpGR_dsL_eff(sL_eff, y.pGR, y.xnCG, y.T),
        dxnCG_dsL_eff(sL_eff, y.pGR, y.xnCG, y.T),
        dT_dsL_eff(sL_eff, y.pGR, y.xnCG, y.T)};
}

void forward_euler(double const t_min, double const t_max, const double dt, Y const y_0, std::size_t const output_every)
{
    std::size_t const max_steps = ceil(((t_max - t_min) + 1e-16) / dt);

    std::cerr << "Forward Euler from " << t_min
              << " to " << t_max
              << " with step size " << dt
              << " (max steps: " << max_steps << ')'
              << '\n';

    Y y{y_0};
    Y::print_head();
    double t = t_min;

    std::size_t output_every_ = output_every;

    for (std::size_t step = 0;
         step < max_steps;
         ++step, t = t_min + step * dt)
    {
        if (t < 1e-3)
        {
            output_every_ = std::max(std::size_t{1}, output_every / 10);
        }
        if (t < 1e-4)
        {
            output_every_ = 1;
        }
        if (step % output_every_ == 0)
        {
            y.print(t);
        }

        Y const dydt = eval_dydt(t, y);

#if 0
        if (dydt.z * dt >= 0.01) {
            // Numerical integration of the ODE is too unstable. Stop here.
            return;
        }
#endif

        Y const dy = dt * dydt;
        y += dy;

        if (dy.z > 0.01)
        {
            y.print(t + dt);
        }
    }

    y.print(t);
}

void runge_kutta_4(double const t_min, double const t_max, const double dt, Y const y_0, std::size_t const output_every)
{
    std::size_t const max_steps = ceil(((t_max - t_min) + 1e-16) / dt);

    std::cerr << "Runge-Kutta 4 from " << t_min
              << " to " << t_max
              << " with step size " << dt
              << " (max steps: " << max_steps << ')'
              << '\n';

    auto const [t_min_, t_max_] = std::minmax(t_min, t_max);

    Y y{y_0};
    Y::print_head();
    double t = t_min;

    std::size_t output_every_ = output_every;

    for (std::size_t step = 0;
         step < max_steps;
         ++step,
                     t = t_min + step * dt)
    {
        if (t < 1e-3)
        {
            output_every_ = std::max(std::size_t{1}, output_every / 10);
        }
        if (t < 1e-4)
        {
            output_every_ = 1;
        }

        if (step % output_every_ == 0)
        {
            y.print(t);
        }

        // enforce time limits
        double const t_p_dt = std::clamp(t + dt, t_min_, t_max_);
        double const dt_ = t_p_dt - t;

        Y const k1 = eval_dydt(t, y);
        Y const k2 = eval_dydt(t + 0.5 * dt_, y + (0.5 * dt_) * k1);
        Y const k3 = eval_dydt(t + 0.5 * dt_, y + (0.5 * dt_) * k2);
        Y const k4 = eval_dydt(t_p_dt, y + dt_ * k3);

        double const a = 1. / 6.;
        Y const phi = a * k1 + (2 * a) * k2 + (2 * a) * k3 + a * k4;

        Y const dy = dt_ * phi;

        y += dy;

        if (dy.z > 0.01)
        {
            y.print(t_p_dt);
        }
    }

    double const t_ = std::clamp(t, t_min_, t_max_);
    y.print(t_);
}

int main()
{
    feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);

    // initial condition
    Y y0{
        // z, pGR, xnCG, T
        0., 101325, 66666666, 365};
    const double pCap_0 = 5555.;
    y0.xnCG = 1. - partial_pressure_vapour(y0.pGR, pCap_0, 0., y0.T) / y0.pGR;

    // initial effective saturation
    const double sL_eff_0 = saturation_effective(pCap_0);

    // integration boundaries
    const double sL_eff_hig = 1e-16;
    const double sL_eff_low = sL_eff_0;

    // step size for the numerical integration
    const int steps = 1000000;
    const double dsL_eff = (sL_eff_hig - sL_eff_low) / steps;

    const int number_of_outputs = 100;
    const int output_every = steps / number_of_outputs;

#if 1
    forward_euler(sL_eff_0, sL_eff_hig, dsL_eff, y0, output_every);
#else
    runge_kutta_4(sL_eff_0, sL_eff_hig, dsL_eff, y0, output_every);
#endif

    return 0;
}
