# Execution times

## Norbert's heat pipe example, SparseLU

* not MKL
* 200 linear quad elements, 402 nodes
* length of the domain: 1.5 m
* 10009 timesteps
* last time: 100000000.0 s

### envinf2

* envinf2 (simulations by other people running concurrently)
* exec time: 4300 s

```
info: The whole computation of the time stepping took 10009 steps, in which
	 the accepted steps are 10009, and the rejected steps are 0.

info: [time] Output of timestep 10009 took 0.0102026 s.
info: [time] Execution took 4321.99 s.
info: OGS terminated on 2022-01-26 17:15:48+0100.
```

### envinf3

* envinf3
* exec time: 2700 s

```
info: Convergence criterion, component 4: |dx|=0.0000e+00, |x|=0.0000e+00, |dx|/|x|=nan
info: [time] Iteration #1 took 0.221851 s.                                             
info: [time] Solving process #0 took 0.221855 s in time step #10009                    
info: [time] Time step #10009 took 0.225901 s.                                         
info: The whole computation of the time stepping took 10009 steps, in which            
         the accepted steps are 10009, and the rejected steps are 0.                   
                                                                                       
info: [time] Output of timestep 10009 took 0.00688955 s.                               
info: [time] Execution took 2723.89 s.                                                 
info: OGS terminated on 2022-01-26 16:50:20+0100.                                      
```

## Norbert's heat pipe example, PardisoLU, Intel MKL

* 200 linear quad elements, 402 nodes
* length of the domain: 1.5 m
* 10009 timesteps
* CMDLINE: OMP_NUM_THREADS=1 "$ogs" -o out/ hp_TH2M_pardiso.prj 2>&1 | tee out/ogs_pardiso.log
* Pardiso with >1 OpenMP threads does not converge anymore
* Apparently no performance gain for these small equation systems

### envinf2

### envinf3

* exec time: 2700 s
* NO ADVANTAGE!

```
[lehmannc@envinf3 TH2M]$ tail out/ogs_pardiso.log                                      
info: Convergence criterion, component 4: |dx|=0.0000e+00, |x|=0.0000e+00, |dx|/|x|=nan
info: [time] Iteration #1 took 0.228714 s.                                             
info: [time] Solving process #0 took 0.228809 s in time step #10009                    
info: [time] Time step #10009 took 0.232869 s.                                         
info: The whole computation of the time stepping took 10009 steps, in which            
         the accepted steps are 10009, and the rejected steps are 0.                   
                                                                                       
info: [time] Output of timestep 10009 took 0.00678803 s.                               
info: [time] Execution took 2723.76 s.                                                 
info: OGS terminated on 2022-01-27 08:50:01+0100.                                      
```
