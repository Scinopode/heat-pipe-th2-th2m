from itertools import cycle

import math

import pyvista as pv
import numpy as np
import matplotlib.pyplot as plt

def read_mesh_along_x_axis(mesh):
    xmin, xmax, ymin, ymax, zmin, zmax = mesh.bounds
    eps = 1e-16
    line = pv.Line((xmin, ymin+eps, zmin), (xmax, ymin+eps, zmin), resolution=2)
    line_mesh = mesh.slice_along_line(line, generate_triangles=True)

    num_pts_mesh = mesh.points.shape[0]
    num_pts_line_mesh = line_mesh.points.shape[0]
    assert 2 * num_pts_line_mesh == num_pts_mesh
    # print(f"num points in line: {line_mesh.points.shape[0]}")

    ys = line_mesh.points[:,1]
    assert (abs(ys - ymin) < 1e-15).all()

    zs = line_mesh.points[:,2]
    assert (abs(zs - zmin) < 1e-15).all()
    
    pd = line_mesh.point_data
    
    return {
        "xs": line_mesh.points[:,0],
        "Ts": pd["temperature"],
        "sLs": pd["saturation"],
        "pGR": pd["gas_pressure"],
        "pLR": pd["liquid_pressure_interpolated"],
        "pCap": pd["capillary_pressure"],
        "vG":  pd["velocity_gas"],
        "vG_x":  pd["velocity_gas"][:,0],
        "vL":  pd["velocity_liquid"],
        "vL_x":  pd["velocity_liquid"][:,0],
        "k_rel_G": pd["k_rel_G"],
        "k_rel_L": pd["k_rel_L"],
        "rhoG":  pd["gas_density"],
        "pVapSat":  pd["vapour_pressure"],
        "xnCG":  pd["xnCG"],
        "pVapPartial": (1. - pd["xnCG"]) * pd["gas_pressure"]
    }

def plot_sL_T_numerical(mesh, nodal_qtys, res, *, plot_nodes=False):
    xmin, xmax, *other = mesh.bounds
    
    if False:
        # reuse color
        hs_l, ls_l = res.get_legend_handles_labels()
        assert len(hs_l) == 1
        color_sL = hs_l[0].get_color()
    
    xs = nodal_qtys["xs"]
    Ts = nodal_qtys["Ts"]
    sLs = nodal_qtys["sLs"]
    
    res.plot(xs, sLs, label="sL (num)")
    res.right_ax.plot(xs, Ts, label="T (num)")
    
    if plot_nodes:
        res.scatter(xs, 0.005 * (np.arange(len(xs)) % 5), 2, marker=".")
    
    res.set_xlim(xmin, xmax)
    res.set_ylim(0, 1)
    
    hs_l, ls_l = res.get_legend_handles_labels()
    hs_r, ls_r = res.right_ax.get_legend_handles_labels()
    res.legend(hs_l + hs_r, ls_l + ls_r)

def plot_sL_T_numerical_multi(mesh, nodal_qtyss, res, ts, t_steps, *, plot_nodes=False):
    assert len(nodal_qtyss) == len(ts)
    assert len(nodal_qtyss) == len(t_steps)
    
    xmin, xmax, *other = mesh.bounds
    
    if False:
        # reuse color
        hs_l, ls_l = res.get_legend_handles_labels()
        assert len(hs_l) == 1
        color_sL = hs_l[0].get_color()
    
    h_sL = None
    h_T = None
    
    line_styles_ = ["-","--","-.",":"]
    line_styles = cycle(line_styles_)
    
    for i in range(len(nodal_qtyss)):
        j = len(nodal_qtyss) - i - 1
        nodal_qtys = nodal_qtyss[j]
        t = ts[j]
        t_step = t_steps[j]

        xs = nodal_qtys["xs"]
        Ts = nodal_qtys["Ts"]
        sLs = nodal_qtys["sLs"]
        
        line_style = next(line_styles)

        h_sL = res.plot(xs, sLs, label=f"sL (num, t={t:.1g} s, ts=#{t_step})",
                        color=h_sL[0].get_color() if h_sL is not None else None,
                        ls=line_style)
        h_T = res.right_ax.plot(xs, Ts, label=f"T (num, t={t:.1g} s, ts=#{t_step})",
                                color=h_T[0].get_color() if h_T is not None else None,
                                ls=line_style)

    if plot_nodes:
        res.scatter(xs, 0.005 * (np.arange(len(xs)) % 5), 2, marker=".")

    res.set_xlim(xmin, xmax)
    res.set_ylim(0, 1)
    
    fig = res.get_figure()
    fig.subplots_adjust(right=0.8)

    hs_l, ls_l = res.get_legend_handles_labels()
    hs_r, ls_r = res.right_ax.get_legend_handles_labels()
    res.legend(hs_l + hs_r, ls_l + ls_r, loc="upper left", bbox_to_anchor=(1.1, 1.0))
    

def plot_pGR_pCap_numerical_multi(mesh, nodal_qtyss, res, ts, t_steps):
    _plot_l_r_numerical_multi(mesh, nodal_qtyss, res, ts, t_steps, left="pGR", left2="pVapPartial", right="pCap")
    
def plot_krel_L_G_numerical_multi(mesh, nodal_qtyss, res, ts, t_steps):
    _plot_l_r_numerical_multi(mesh, nodal_qtyss, res, ts, t_steps, left="k_rel_L", right="k_rel_G")
    
def plot_rhoG_numerical_multi(mesh, nodal_qtyss, res, ts, t_steps):
    _plot_l_numerical_multi(mesh, nodal_qtyss, res, ts, t_steps, left="rhoG")
    
def plot_pVap_numerical_multi(mesh, nodal_qtyss, res, ts, t_steps):
    _plot_ll_numerical_multi(mesh, nodal_qtyss, res, ts, t_steps, left1="pVapSat", left2="pVapPartial")

def _plot_l_r_numerical_multi(mesh, nodal_qtyss, res, ts, t_steps, *, left, left2=None, right):
    assert len(nodal_qtyss) == len(ts)
    assert len(nodal_qtyss) == len(t_steps)
    
    xmin, xmax, *other = mesh.bounds
    
    h_left = None
    h_left2 = None
    h_right = None
    
    left_var = left
    left2_var = left2
    right_var = right
    
    line_styles_ = ["-","--","-.",":"]
    line_styles = cycle(line_styles_)
    
    for i in range(len(nodal_qtyss)):
        j = len(nodal_qtyss) - i - 1
        nodal_qtys = nodal_qtyss[j]
        t = ts[j]
        t_step = t_steps[j]

        xs = nodal_qtys["xs"]
        left_vals = nodal_qtys[left_var]
        left2_vals = nodal_qtys[left2_var] if left2_var is not None else None
        right_vals = nodal_qtys[right_var]
        
        line_style = next(line_styles)

        h_left = res.plot(xs, left_vals, label=f"{left_var} (num, t={t:.1g} s, ts=#{t_step})",
                        color=h_left[0].get_color() if h_left is not None else None,
                        ls=line_style)
        if left2 is not None:
            h_left2 = res.plot(xs, left2_vals, label=f"{left2_var} (num, t={t:.1g} s, ts=#{t_step})",
                            color=h_left2[0].get_color() if h_left2 is not None else None,
                            ls=line_style)
        h_right = res.right_ax.plot(xs, right_vals, label=f"{right_var} (num, t={t:.1g} s, ts=#{t_step})",
                                color=h_right[0].get_color() if h_right is not None else None,
                                ls=line_style)

    # plot mesh nodes
    # res.scatter(xs, 0.005 * (np.arange(len(xs)) % 5), 2, marker=".")

    res.set_xlim(xmin, xmax)
    #res.set_ylim(0, 1)
    res.set_ylabel(left_var)
    res.right_ax.set_ylabel(right_var)
    
    fig = res.get_figure()
    fig.subplots_adjust(right=0.8)

    hs_l, ls_l = res.get_legend_handles_labels()
    hs_r, ls_r = res.right_ax.get_legend_handles_labels()
    res.legend(hs_l + hs_r, ls_l + ls_r, loc="upper left", bbox_to_anchor=(1.1, 1.0))

 
    
def _plot_l_numerical_multi(mesh, nodal_qtyss, res, ts, t_steps, *, left):
    assert len(nodal_qtyss) == len(ts)
    assert len(nodal_qtyss) == len(t_steps)
    
    xmin, xmax, *other = mesh.bounds
    
    h_left = None
    #h_right = None
    
    left_var = left
    #right_var = right
    
    line_styles_ = ["-","--","-.",":"]
    line_styles = cycle(line_styles_)
    
    for i in range(len(nodal_qtyss)):
        j = len(nodal_qtyss) - i - 1
        nodal_qtys = nodal_qtyss[j]
        t = ts[j]
        t_step = t_steps[j]

        xs = nodal_qtys["xs"]
        left_vals = nodal_qtys[left_var]
        #right_vals = nodal_qtys[right_var]
        
        line_style = next(line_styles)

        h_left = res.plot(xs, left_vals, label=f"{left_var} (num, t={t:.1g} s, ts=#{t_step})",
                        color=h_left[0].get_color() if h_left is not None else None,
                        ls=line_style)
        #h_right = res.right_ax.plot(xs, right_vals, label=f"{right_var} (num, t={t:.1g} s, ts=#{t_step})",
        #                        color=h_right[0].get_color() if h_right is not None else None,
        #                        ls=line_style)

    # plot mesh nodes
    # res.scatter(xs, 0.005 * (np.arange(len(xs)) % 5), 2, marker=".")

    res.set_xlim(xmin, xmax)
    #res.set_ylim(0, 1)
    res.set_ylabel(left_var)
    #res.right_ax.set_ylabel(right_var)
    
    fig = res.get_figure()
    fig.subplots_adjust(right=0.8)

    hs_l, ls_l = res.get_legend_handles_labels()
    #hs_r, ls_r = res.right_ax.get_legend_handles_labels()
    #res.legend(hs_l + hs_r, ls_l + ls_r, loc="upper left", bbox_to_anchor=(1.1, 1.0))
    res.legend(hs_l, ls_l, loc="upper left", bbox_to_anchor=(1.1, 1.0))
    
    
def _plot_ll_numerical_multi(mesh, nodal_qtyss, res, ts, t_steps, *, left1, left2):
    assert len(nodal_qtyss) == len(ts)
    assert len(nodal_qtyss) == len(t_steps)
    
    xmin, xmax, *other = mesh.bounds
    
    h_left1 = None
    h_left2 = None
    
    left1_var = left1
    left2_var = left2
    
    line_styles_ = ["-","--","-.",":"]
    line_styles = cycle(line_styles_)
    
    for i in range(len(nodal_qtyss)):
        j = len(nodal_qtyss) - i - 1
        nodal_qtys = nodal_qtyss[j]
        t = ts[j]
        t_step = t_steps[j]

        xs = nodal_qtys["xs"]
        left1_vals = nodal_qtys[left1_var]
        left2_vals = nodal_qtys[left2_var]
        
        line_style = next(line_styles)

        h_left1 = res.plot(xs, left1_vals, label=f"{left1_var} (num, t={t:.1g} s, ts=#{t_step})",
                        color=h_left1[0].get_color() if h_left1 is not None else None,
                        ls=line_style)
        h_left2 = res.plot(xs, left2_vals, label=f"{left2_var} (num, t={t:.1g} s, ts=#{t_step})",
                           color=h_left2[0].get_color() if h_left2 is not None else None,
                           ls=line_style)

    # plot mesh nodes
    # res.scatter(xs, 0.005 * (np.arange(len(xs)) % 5), 2, marker=".")

    res.set_xlim(xmin, xmax)
    res.set_ylabel(f"{left1_var}, {left2_var}")
    
    fig = res.get_figure()
    fig.subplots_adjust(right=0.8)

    hs_l, ls_l = res.get_legend_handles_labels()
    res.legend(hs_l, ls_l, loc="upper left", bbox_to_anchor=(1.1, 1.0))
    
    
def plot_v_numerical_multi(mesh, nodal_qtyss, ts, t_steps):
    assert len(nodal_qtyss) == len(ts)
    assert len(nodal_qtyss) == len(t_steps)
    
    xmin, xmax, *other = mesh.bounds
    
    h_left = None
    h_right = None
    
    left_var = "vG"
    right_var = "vL"
    
    line_styles_ = ["-","--","-.",":"]
    line_styles = cycle(line_styles_)
    
    fig, ax = plt.subplots(figsize=(12,6))
    ax2 = ax.twinx()
    
    for i in range(len(nodal_qtyss)):
        j = len(nodal_qtyss) - i - 1
        nodal_qtys = nodal_qtyss[j]
        t = ts[j]
        t_step = t_steps[j]

        xs = nodal_qtys["xs"]
        left_vals = nodal_qtys[left_var][:,0] # x component only
        right_vals = nodal_qtys[right_var][:,0]
        
        line_style = next(line_styles)

        h_left = ax.plot(xs, left_vals, label=f"{left_var} (num, t={t:.1g} s, ts=#{t_step})",
                        color=h_left[0].get_color() if h_left is not None else "tab:red",
                        ls=line_style)
        h_right = ax2.plot(xs, right_vals, label=f"{right_var} (right, num, t={t:.1g} s, ts=#{t_step})",
                                color=h_right[0].get_color() if h_right is not None else "tab:blue",
                                ls=line_style)

    # plot mesh nodes
    # res.scatter(xs, 0.005 * (np.arange(len(xs)) % 5), 2, marker=".")
    
    ax.set_xlim(xmin, xmax)
    
    ax.tick_params(axis='y', labelcolor=h_left[0].get_color())
    ax2.tick_params(axis='y', labelcolor=h_right[0].get_color())
    
    #res.set_ylim(0, 1)
    
    fig.subplots_adjust(right=0.8)

    hs_l, ls_l = ax.get_legend_handles_labels()
    hs_r, ls_r = ax2.get_legend_handles_labels()
    ax.legend(hs_l + hs_r, ls_l + ls_r, loc="upper left", bbox_to_anchor=(1.1, 1.0))
    
    return fig
    

def _plot_compare_l_or_r(*, data, var_names, labels, cycler, l_or_r, ax):
    assert len(labels) == len(data)
    
    hs = [ None ] * len(var_names)
    
    line_styles_ = ["-","--","-.",":"]
    line_styles = cycle(line_styles_)
    
    for label, data_ in zip(labels, data):
        xs = data_["xs"]
        ls = next(line_styles)
        
        for var_idx, var_name in enumerate(var_names):
            vals = data_[var_name]
            assert len(vals.shape) == 1 or vals.shape[1] == 1

            h = hs[var_idx]
            color = next(cycler)["color"] if h is None else h.get_color()
            
            h, = ax.plot(xs, vals, label=f"{l_or_r} {label}: {var_name}", ls=ls, color=color)
            
            hs[var_idx] = h
        
    
def plot_compare(*, mesh, data, left, right=[], labels):
    """
    data: [ { "xs": [...], "Ts": [...], ... }, ... ]
    left, right: [ "Ts", ... ]
    labels: [ "name of 1st data set", "name of 2nd data set", ... ]
    
    encodes:
    var name -> color
    data set -> line style
    """
    
    assert len(labels) == len(data)
    
    fig, ax = plt.subplots(1)
    ax2 = None
    
    # shared property cycler
    # TODO maybe restrict to colors
    cycler = plt.rcParams['axes.prop_cycle']()
    
    if len(left) != 0:
        _plot_compare_l_or_r(data=data, var_names=left, labels=labels, l_or_r="←", cycler=cycler, ax=ax)
    
    if len(right) != 0:
        ax2 = ax.twinx()
        _plot_compare_l_or_r(data=data, var_names=right, labels=labels, l_or_r="→", cycler=cycler, ax=ax2)

    xmin, xmax, *other = mesh.bounds
    ax.set_xlim(xmin, xmax)
    ax.set_xlabel("$x$ / m")
    
    fig.subplots_adjust(right=0.8)

    hs_l, ls_l = ax.get_legend_handles_labels()
    hs_r = []; ls_r = []
    if ax2:
        hs_r, ls_r = ax2.get_legend_handles_labels()
    ax.legend(hs_l + hs_r, ls_l + ls_r, loc="upper left", bbox_to_anchor=(1.1, 1.0))
    
    return fig
    
    
def read_n_meshes(reader, n):
    t_max = reader.time_values[-1]
    n_ts = len(reader.time_values)
    
    print(f"last time: {t_max} s (#{n_ts})")
    step = n_ts // n
    
    t_steps = [0] * n
    for i in range(n):
        t_steps[-i-1] = n_ts - 1 - i * step
    
    ts = [ 0.] * n
    meshes = [ None ] * n
    for i, step in enumerate(t_steps):
        reader.set_active_time_point(step)
        ts[i] = reader.reader.active_time_value
        meshes[i] = reader.read()[0]
    
    return t_steps, ts, meshes

def read_meshes(reader, times_relative):
    t_max = reader.time_values[-1]
    n_ts = len(reader.time_values)
    t_steps = [ int(math.ceil(n_ts * perc)) - 1 for perc in times_relative ]
    
    ts = [ 0.] * len(t_steps)
    meshes = [ None ] * len(t_steps)
    for i, step in enumerate(t_steps):
        reader.set_active_time_point(step)
        ts[i] = reader.reader.active_time_value
        meshes[i] = reader.read()[0]
    
    return t_steps, ts, meshes